//
//  GameScene.swift
//  BabaIsYou-F19
//
//  Created by Parrot on 2019-10-17.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var baba = SKSpriteNode()
    var wall = SKSpriteNode()
    var wallBlock = SKSpriteNode()
    
    var flag = SKSpriteNode()
    var flagBlock = SKSpriteNode()
    
    var isBlock = SKSpriteNode()
    var stopBlock = SKSpriteNode()
    var winBlock = SKSpriteNode()
    var status = SKLabelNode()

    
    var message = SKLabelNode()
    
    var wallRuleActive:Bool = false
    var flagRuleActive: Bool = false
    var winRuleActive: Bool = false
    //var ruleChanged: Bool = false
    
    let PLAYER_DISPLACEMENT:CGFloat = 5
    
    
    
    override func didMove(to view: SKView) {
        
        self.physicsWorld.contactDelegate = self
        
        self.status = self.childNode(withName: "status") as! SKLabelNode

        self.baba = self.childNode(withName: "baba") as! SKSpriteNode
        
        self.wallBlock = self.childNode(withName: "wallblock") as! SKSpriteNode
        
        self.flagBlock = self.childNode(withName: "flagblock") as! SKSpriteNode
        
        self.flag = self.childNode(withName: "flag") as! SKSpriteNode
        
        //self.isBlock = self.childNode(withName: "isblock") as! SKSpriteNode
        
        self.stopBlock = self.childNode(withName: "stopblock") as! SKSpriteNode
        
        self.winBlock = self.childNode(withName: "winblock") as! SKSpriteNode
        
        print("Initial check")
        
        
        
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        
        if !wallRuleActive {
            print("Initial check")
            isWallRuleActiveOnLoad()
        }
        
        if !winRuleActive {
            print("Initial check")
            isWinRuleActiveOnLoad()               }
        
        print("Collision Detected")
        let nodeA = contact.bodyA.node
        let nodeB = contact.bodyB.node
        
        if (nodeA == nil || nodeB == nil) {
            return
        }
        
        
        
        if nodeA!.name == "baba" && nodeB!.name == "stopblock" {
            print("StopBlock-Baba")
            if wallRuleActive {
                print("Inside ded begin check")
                self.wallRuleActive = false
                self.baba.physicsBody?.collisionBitMask = PhysicsCategory.blockCategory
                self.wall.physicsBody?.isDynamic = true
                self.wall.physicsBody?.collisionBitMask = PhysicsCategory.none
            }
        }
        
        if nodeA!.name == "baba" && nodeB!.name == "wallblock" {
            print("Wallblock-Baba")
            if wallRuleActive {
                print("Inside did begin check Wallblock-Baba")
                self.wallRuleActive = false
                self.baba.physicsBody?.collisionBitMask = PhysicsCategory.blockCategory
                self.wall.physicsBody?.isDynamic = true
                self.wall.physicsBody?.collisionBitMask = PhysicsCategory.none
            }
        }
        
        if !wallRuleActive {
            if nodeA!.name == "wallblock" && nodeB!.name == "isblock" || nodeA!.name == "isblock" && nodeB!.name == "wallblock"{
                print("Wallblock-isBlock")
                if wallRuleActive {
                    print("Inside did begin check Wallblock-isBlock")
                    self.wallRuleActive = false
                    self.baba.physicsBody?.collisionBitMask = PhysicsCategory.blockCategory
                    self.wall.physicsBody?.isDynamic = true
                    self.wall.physicsBody?.collisionBitMask = PhysicsCategory.none
                }else {
                    print("Inside else of Wallblock-isBlock")
                    isWallRuleActiveOnLoad()
                }
            }
            
            if nodeA!.name == "stopblock" && nodeB!.name == "isblock" || nodeA!.name == "isblock" && nodeB!.name == "stopblock" {
                print("Stopblock-isBlock")
                
                if wallRuleActive  {
                    print("Inside did begin check Wallblock-isBlock")
                    self.wallRuleActive = false
                    self.baba.physicsBody?.collisionBitMask = PhysicsCategory.blockCategory
                    self.wall.physicsBody?.isDynamic = true
                    self.wall.physicsBody?.collisionBitMask = PhysicsCategory.none
                }else {
                    print("Inside else of Stopblock-isBlock")
                    isWallRuleActiveOnLoad()
                }
            }
        }
        
        if nodeA!.name == "baba" && nodeB!.name == "winblock" {
            print("winblock-Baba")
            if winRuleActive {
                status.text = ""
                print("Inside did begin check")
                self.winRuleActive = false
                self.baba.physicsBody?.contactTestBitMask = PhysicsCategory.blockCategory
            }
        }
        
        if nodeA!.name == "baba" && nodeB!.name == "flagblock" {
                 print("flagblock-Baba")
                 if winRuleActive {
                     status.text = ""
                     print("Inside did begin check")
                     self.winRuleActive = false
                     self.baba.physicsBody?.contactTestBitMask = PhysicsCategory.blockCategory
                 }
             }
             
        
        if nodeA!.name == "winBlock" && nodeB!.name == "isblock" || nodeA!.name == "isblock" && nodeB!.name == "winBlock" {
            print("winBlock-isBlock")
            print("Inside did begin check winBlock-isBlock")
            isWinRuleActiveOnLoad()
            
        }
        
        if nodeA!.name == "flagblock" && nodeB!.name == "isblock" || nodeA!.name == "isblock" && nodeB!.name == "flagblock" {
            print("flagblock-isBlock")
            print("Inside did begin check flagblock-isBlock")
            isWinRuleActiveOnLoad()
            
        }
        
        
        if nodeA!.name == "baba" && nodeB!.name == "flag" || nodeA!.name == "flag" && nodeB!.name == "baba" {
            print("baba-flag")
            print("Inside did begin check baba-flag")
            if winRuleActive {
                status.text = "Game won"
                print("Game won")
            }
        }
        
        
    }
    
    struct PhysicsCategory {
        static let none: UInt32 = 0
        static let babaCategory: UInt32 = 0b1
        static let wallCategory: UInt32 = 0b10
        static let blockCategory: UInt32 = 0b100
        static let flagCategory: UInt32 = 0b1000
        
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if (touch == nil) {
            return
        }
        let touchLocation = touch!.location(in: self)
        
        let touchedNode = atPoint(touchLocation).name
        
        if (touchedNode == "upButton") {
            self.baba.position.y = self.baba.position.y + PLAYER_DISPLACEMENT
        }
        else if (touchedNode == "downButton") {
            self.baba.position.y = self.baba.position.y - PLAYER_DISPLACEMENT
        }
        else if (touchedNode == "leftButton") {
            self.baba.position.x = self.baba.position.x - PLAYER_DISPLACEMENT
        }
        else if (touchedNode == "rightButton") {
            self.baba.position.x = self.baba.position.x + PLAYER_DISPLACEMENT
        }
    }
    
    
    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    //Check whether if WALL IS STOP is active
    func isWallRuleActiveOnLoad(){
        print("Inside isWallRuleActiveOnLoad")
        self.enumerateChildNodes(withName: "isblock"){
            (node, stop) in
            self.isBlock = node as SKNode as! SKSpriteNode
            if ((round(self.wallBlock.position.y) == round(self.isBlock.position.y)) && (round(self.isBlock.position.y) == round(self.stopBlock.position.y))  && (self.wallBlock.position.x + 65 > self.isBlock.position.x) &&  (self.isBlock.position.x + 65 > self.stopBlock.position.x)) {
                print("Inside isWallRule: All the blocks are aligned in same vertical")
                self.baba.physicsBody?.collisionBitMask = PhysicsCategory.blockCategory | PhysicsCategory.wallCategory
                self.wallRuleActive = true
                self.enumerateChildNodes(withName: "wall") {
                    (node, stop) in
                    self.wall = node as! SKSpriteNode
                    self.wall.physicsBody?.isDynamic = false
                    self.wall.physicsBody?.collisionBitMask = PhysicsCategory.babaCategory | PhysicsCategory.blockCategory
                }
            }
            
        }
    }
    
    //Check whether if FLAG IS STOP is active
    func isFlagRuleActiveOnLoad() {
        print("Inside isFlagRuleActiveOnLoad")
        self.enumerateChildNodes(withName: "isblock"){
            (node, stop) in
            self.isBlock = node as SKNode as! SKSpriteNode
            if ((round(self.flagBlock.position.y) == round(self.isBlock.position.y)) && (round(self.isBlock.position.y) == round(self.stopBlock.position.y))) {
                print("Inside isFlagRuleActiveOnLoad: All the block are aligned in same y and x")
                if (self.flagBlock.position.x + 65 > self.isBlock.position.x) &&
                    (self.isBlock.position.x + 65 >  self.stopBlock.position.x) {
                    print("Condition valid")
                    self.baba.physicsBody?.collisionBitMask = PhysicsCategory.blockCategory | PhysicsCategory.flagCategory
                    self.flagRuleActive = true
                    self.flag.physicsBody?.isDynamic = false
                    self.flag.physicsBody?.collisionBitMask = PhysicsCategory.babaCategory
                }
            }
        }
    }
    
    //Check whether if FLAG IS WIN is active
    func isWinRuleActiveOnLoad() {
        print("Inside isWinRuleActiveOnLoad")
        self.enumerateChildNodes(withName: "isblock"){
            (node, stop) in
            self.isBlock = node as SKNode as! SKSpriteNode
            print(round(self.flagBlock.position.y))
            print(round(self.isBlock.position.y))
            print(round(self.winBlock.position.y))
            
            if ((round(self.flagBlock.position.y) == round(self.isBlock.position.y)) && (round(self.isBlock.position.y) == round(self.winBlock.position.y))  && (self.flagBlock.position.x + 65 > self.isBlock.position.x) &&  (self.isBlock.position.x + 65 > self.winBlock.position.x)) {
                print("Inside isWinRuleActiveOnLoad: All the blocks are aligned in same vertical and horizontal")
                self.baba.physicsBody?.contactTestBitMask = PhysicsCategory.blockCategory | PhysicsCategory.flagCategory
                self.winRuleActive = true
                
            }
            
        }
        
    }
    
    //Check whether if WALL IS WIN is active
    func isWallWinRuleActiveOnLoad() {
           print("Inside isWinRuleActiveOnLoad")
           self.enumerateChildNodes(withName: "isblock"){
               (node, stop) in
               self.isBlock = node as SKNode as! SKSpriteNode
               print(round(self.wallBlock.position.y))
               print(round(self.isBlock.position.y))
               print(round(self.winBlock.position.y))
               
               if ((round(self.wallBlock.position.y) == round(self.isBlock.position.y)) && (round(self.isBlock.position.y) == round(self.winBlock.position.y))  && (self.flagBlock.position.x + 65 > self.isBlock.position.x) &&  (self.isBlock.position.x + 65 > self.winBlock.position.x)) {
                   print("Inside isWinRuleActiveOnLoad: All the blocks are aligned in same vertical and horizontal")
                   self.baba.physicsBody?.contactTestBitMask = PhysicsCategory.blockCategory | PhysicsCategory.flagCategory
                   self.winRuleActive = true
                   
               }
               
           }
           
       }
}
